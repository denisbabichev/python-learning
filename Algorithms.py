from functools import lru_cache


def bad_fib(num):
    # resolve with recursion
    if num == 0:
        return 0
    elif num == 1:
        return 1
    else:
        return bad_fib(num - 2) + bad_fib(num - 1)


def better_fib(num):
    # much faster count of Fibonacci number
    num1, num2 = 0, 1
    for _ in range(1, num):
        num1, num2 = num2, num1 + num2
    return num2


# memo is a wrapper function for hidding cache from global scope
def memo(func):
    # dict for caching last fib num
    cache = {}

    def inner(n):
        if n not in cache:
            cache[n] = func(n)
        return cache[n]

    return inner


cached_fib = lru_cache(bad_fib)  # same thing as memo but in builtin python modules


print(better_fib(20577))